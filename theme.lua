---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local theme = {}

theme.font          = "Ubuntu Condensed 11.5"

theme.bg_normal     = "#ffffff"

theme.bg_focus      = "#00bcd4"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#000000"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(0)
theme.border_width  = dpi(4)
theme.border_normal = "#ffffff"
theme.border_focus  = "#00bcd4"
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(25)
theme.menu_width  = dpi(125)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"


-- You can use your own layout icons like this:
local icondir = ""

-- Fort Awesome

icondir = "~/.config/awesome/Font-Awesome/png20/"

theme.widget_example      = icondir .. "cat.png"
theme.widget_wifi         = icondir .. "wifi.png"
theme.widget_temp         = icondir .. "thermometer-half.png"
theme.widget_uptime       = icondir .. "charging-station.png"
theme.widget_cpu          = icondir .. "microchip.png"
theme.widget_fs           = icondir .. "hdd.png"
theme.widget_mem          = icondir .. "memory.png"
theme.widget_note         = icondir .. "music.png"
theme.widget_batt         = icondir .. "battery-full.png"
theme.widget_clock        = icondir .. "clock.png"
theme.widget_vol          = icondir .. "volume-up.png"
theme.widget_weather      = icondir .. "cloud.png"
theme.widget_netdown      = icondir .. "download.png"
theme.widget_netup        = icondir .. "upload.png"

theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"


theme.monitor_disk        = icondir .. "hdd.png"
theme.monitor_vol         = icondir .. "volume-up.png"
theme.monitor_vol_low     = icondir .. "volume-down.png"
theme.monitor_vol_no      = icondir .. "volume-off.png"
theme.monitor_vol_mute    = icondir .. "volume-mute..png"

theme.monitor_ac          = icondir .. "charging-station.png"
theme.monitor_bat         = icondir .. "battery-three-quarters.png"
theme.monitor_bat_low     = icondir .. "battery-quarters.png"
theme.monitor_bat_no      = icondir .. "battery-empty.png"
-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Breeze"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
